# PW

#### 介绍
wpf

#### 软件架构
软件架构说明
Visual Studio 2015
.NET Framework 4.6

Prism.Core 7.0
Prism.Mef 6.3
Prism.Wpf 6.3

EntityFramework 6.2
MySql.Data 6.10.9

#### 安装教程

1.  初始化NuGet包
2.  安装EF需要安装文件夹下程序
3.  单独生成各模块（Aside、Chat、Footer、SystemHeader、Map、SystemSet、Tools）
4.  运行PW.Desktop

数据库暂无

#### 预览
[https://blog.csdn.net/shishuwei111/article/details/82933075](https://blog.csdn.net/shishuwei111/article/details/82933075)
![登录](https://images.gitee.com/uploads/images/2020/0727/135659_41bccf8d_545745.png "20181003180923780.png")
![主导航](https://images.gitee.com/uploads/images/2020/0727/135737_75a7071c_545745.png "20181003181111256.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0727/135759_559ad54e_545745.png "20181003181145876.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0727/135809_0e59e5a4_545745.png "20181003181214849.png")

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
